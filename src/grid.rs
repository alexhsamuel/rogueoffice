use std::cmp::{min, max};

#[derive(Clone, Copy, Debug)]
pub struct Pos(pub i32, pub i32);

impl Pos {
    pub fn moved(&self, dx: i32, dy: i32) -> Self {
        let Self(x, y) = self;
        Self(x + dx, y + dy)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct PosRect(pub i32, pub i32, pub i32, pub i32);

impl PosRect {
    pub fn contains(&self, pos: &Pos) -> bool {
        let Pos(x, y) = *pos;
        self.0 <= x && x < self.2 && self.1 <= y && y < self.3
    }

    pub  fn intersect(&self, rect: &PosRect) -> Self {
        Self(
            max(self.0, rect.0),
            max(self.1, rect.1),
            min(self.2, rect.2),
            min(self.3, rect.3),
        )
    }
}

//------------------------------------------------------------------------------

#[derive(Clone, Debug)]  // FIXME: Remove.
pub struct Grid<T> {
    pub rect: PosRect,
    grid: Vec<T>,
}

#[derive(Debug)]
pub struct PosError(Pos);

impl<T> Grid<T> {
    pub fn new<F: FnMut() -> T>(rect: PosRect, default: F) -> Self{
        let PosRect(x0, y0, x1, y1) = rect;
        let n = (x1 - x0) * (y1 - y0);
        assert!(n >= 0);
        Self {
            rect,
            grid: {
                let mut vec = Vec::with_capacity(n as usize);
                vec.resize_with(n as usize, default);
                vec
            },
        }
    }

    fn index(&self, pos: Pos) -> usize {
        assert!(self.rect.contains(&pos));
        let Pos(x, y) = pos;
        let PosRect(x0, y0, x1, _y1) = self.rect;
        ((x - x0) + (y - y0) * (x1 - x0)) as usize
    }

    pub fn get(&self, pos: Pos) -> &T {
        &self.grid[self.index(pos)]
    }

    pub fn get_mut(&mut self, pos: Pos) -> &mut T {
        let i = self.index(pos);
        &mut self.grid[i]
    }

    pub fn set(&mut self, pos: Pos, val: T) -> T {
        let i = self.index(pos);
        let mut val = val;
        std::mem::swap(&mut val, &mut self.grid[i]);
        val
    }
}

