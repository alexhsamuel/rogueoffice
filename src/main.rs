mod game;
mod grid;
mod view;

use notcurses;
use std::thread;
use std::time::Duration;
use std::sync::mpsc::{channel, Sender, Receiver, RecvTimeoutError};

use crate::game::*;
use crate::grid::*;
use crate::view::*;

use std::fs::File;
use std::io::Write;

const PLAYER_NAME: &str = "player";
const POLL_INTERVAL: Duration = Duration::from_millis(10);
const RECT: PosRect = PosRect(0, 0, 60, 40);

const LOG: bool = true;

fn server(mut game: Game, client: (Receiver<Action>, Sender<Update>)) {
    let (receiver, sender) = client;

    sender.send(update_player_view(&game)).unwrap();

    loop {
        let action = receiver.recv().unwrap();
        let update = apply_action(&mut game, &action);
        sender.send(update).unwrap();
    }
}

fn client(server: (Receiver<Update>, Sender<Action>)) {
    let mut log = if LOG { Some(File::create("./client.log").unwrap()) } else { None };

    let (receiver, sender) = server;

    let mut nc = notcurses::Notcurses::new().unwrap();
    let mut plane = nc.cli_plane().unwrap();

    let mut view = View::new(RECT);

    draw_view(&view, &mut plane).unwrap();
    plane.render().unwrap();

    loop {
        match receiver.recv_timeout(POLL_INTERVAL) {
            Ok(update) => {
                if LOG {
                    writeln!(log.as_mut().unwrap(), "update: {:?}", update).unwrap();
                }
                update_view(&mut view, &update);
                draw_view(&view, &mut plane).unwrap();
                plane.render().unwrap();
            },
            Err(RecvTimeoutError::Timeout) => {},
            Err(err) => {
                if LOG {
                    writeln!(log.as_mut().unwrap(), "receive err: {:?}", err).unwrap();
                }
                panic!();
            }
        }

        let input = nc.poll_event().unwrap();
        if input.received == notcurses::Received::NoInput {
            // FIXME: Don't sleep if we received an update?
            // Nothing.  Try again.
            std::thread::sleep(POLL_INTERVAL);
            continue;
        }
        if LOG {
            writeln!(log.as_mut().unwrap(), "input event: {:?}", input).unwrap();
        }

        // Show the event.
        // plane.cursor_move_to(notcurses::Position(0, 0)).unwrap();
        // let inp = input.to_string();
        // plane.putstr(&inp).unwrap();

        let action = match input.received {
            // FIXME: Server associates channel with player, so we don't have to
            // send the player name on each message.
            notcurses::Received::Char('i') => Action::MoveEntity(PLAYER_NAME.to_string(),  0, -1),
            notcurses::Received::Char('j') => Action::MoveEntity(PLAYER_NAME.to_string(), -1,  0),
            notcurses::Received::Char('k') => Action::MoveEntity(PLAYER_NAME.to_string(),  0,  1),
            notcurses::Received::Char('l') => Action::MoveEntity(PLAYER_NAME.to_string(),  1,  0),
            notcurses::Received::Char('w') => Action::SetCell(PLAYER_NAME.to_string(), '█'),
            notcurses::Received::Char('q') => break,

            _ => continue,
        };
        sender.send(action).unwrap();
    }
}

fn main() -> () {
    // putstrln![plane, "{plane:?}"]?;
    // assert_eq![11, plane.putstr_at(Position(79, 12), "FOO BAR BAZ")?];
    // putstr_at![plane, Position(79, 12), "FOO BAR BAZ"];

    // putstr![plane, "·←cursor:{} ", plane.cursor()]?;
    // putstrln![plane, "·← cursor {}", plane.cursor()]?;

    let mut game = Game::new(RECT);
    add_entity(&mut game,
               Entity { name: PLAYER_NAME.to_string(), sym: '@' },
               Pos(10, 4));

    let actions = channel::<Action>();
    let updates = channel::<Update>();

    let server_thread = thread::spawn(move|| { server(game, (actions.1, updates.0)); });
    let client_thread = thread::spawn(move|| { client((updates.1, actions.0)); });

    server_thread.join().unwrap();
    client_thread.join().unwrap();
}

