use notcurses;

use crate::grid::{Grid, Pos, PosRect};

//------------------------------------------------------------------------------

pub struct View {
    grid: Grid<char>,
}

impl View {
    pub fn new(rect: PosRect) -> Self {
        Self { grid: Grid::new(rect, || ' ') }
    }
}

//------------------------------------------------------------------------------

#[derive(Debug)]
pub enum Update {
    None,
    ViewRect(Grid<char>),
}

pub fn update_view(view: &mut View, update: &Update) {
    match update {
        Update::None => (),
        Update::ViewRect(grid) => {
            let PosRect(x0, y0, x1, y1) = view.grid.rect.intersect(&grid.rect);
            for x in x0 .. x1 {
                for y in y0 .. y1 {
                    let p = Pos(x, y);
                    view.grid.set(p, *grid.get(p));
                }
            };
        },
    }
}

//------------------------------------------------------------------------------

pub fn draw_view(view: &View, plane: &mut notcurses::Plane) -> Result<(), notcurses::Error> {
    let PosRect(x0, y0, x1, y1) = view.grid.rect;
    let xo = x0;
    let yo = y0;

    // FIXME: Crop.
    plane.set_fg(0x306050);
    for y in y0 .. y1 {
        // FIXME: unwrap()
        plane.cursor_move_to(notcurses::Position(2 + x0 - xo, 2 + y - yo))?;
        for x in x0 .. x1 {
            plane.putstr(&view.grid.get(Pos(x, y)).to_string())?;
        }
        plane.cursor_move_to(notcurses::Position(0, 2 + y1 - yo))?;
        plane.putstr(" ")?;
    }
    plane.unset_fg();
    Ok(())
}

