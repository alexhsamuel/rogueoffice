use std::collections::HashMap;

use crate::grid::{Grid, Pos, PosRect};
use crate::view::Update;

//------------------------------------------------------------------------------

pub enum Action {
    MoveEntity(String, i32, i32),
    SetCell(String, char),
}

//------------------------------------------------------------------------------

pub struct Entity {
    pub name: String,
    pub sym: char,
}

//------------------------------------------------------------------------------

pub struct GridCell {
    sym: char,
}

impl GridCell {
    fn new(sym: char) -> Self {
        GridCell { sym, }
    }
}

//------------------------------------------------------------------------------

pub struct Game {
    pub grid: Grid<GridCell>,
    entities: HashMap<String, (Entity, Pos)>,
}

impl Game {
    pub fn new(rect: PosRect) -> Self {
        let PosRect(x0, y0, x1, y1) = rect;
        let n = (x1 - x0) * (y1 - y0);
        assert!(n > 0);
        Self {
            grid: Grid::new(rect, || GridCell::new('.')),
            entities: HashMap::new(),
        }
    }

    pub fn get_entity_pos(&self, name: &str) -> Pos {
        self.entities.get(name).unwrap().1
    }
}

pub fn add_entity(game: &mut Game, entity: Entity, pos: Pos) {
    game.entities.insert(entity.name.clone(), (entity, pos));
}

pub fn move_entity(game: &mut Game, name: &str, pos: Pos) -> Pos {
    let mut old_pos = pos;
    std::mem::swap(&mut game.entities.get_mut(name).unwrap().1, &mut old_pos);
    old_pos
}

//------------------------------------------------------------------------------

const VIEW_DIST: i32 = 3;

pub fn apply_action(game: &mut Game, action: &Action) -> Update {
    match action {
        Action::MoveEntity(name, dx, dy) => {
            // FIXME: Other entities might see this too.
            let new_pos = game.get_entity_pos(name).moved(*dx, *dy);
            if game.grid.rect.contains(&new_pos) && game.grid.get(new_pos).sym != '█' {
                move_entity(game, name, new_pos);
                update_player_view(game)
            } else {
                // Can't go there.
                Update::None
            }
        }

        Action::SetCell(name, new_char) => {
            let pos = game.get_entity_pos(name);
            game.grid.get_mut(pos).sym = *new_char;
            update_player_view(game)
        }
    }
}

pub fn render(game: &Game, rect: &PosRect) -> Grid<char> {
    let rect = rect.intersect(&game.grid.rect);
    let mut grid = Grid::new(rect, || '?');
    let PosRect(x0, y0, x1, y1) = rect.intersect(&game.grid.rect);
    for x in x0..x1 {
        for y in y0..y1 {
            let p = Pos(x, y);
            let cell = game.grid.get(p);
            grid.set(p, cell.sym);
        }
    }

    for (entity, pos) in game.entities.values() {
        if rect.contains(pos) {
            grid.set(*pos, entity.sym);
        }
    }

    grid
}

/// Returns an update for what is currently visible to the player.
pub fn update_player_view(game: &Game) -> Update {
    // FIXME
    let Pos(x, y) = game.get_entity_pos("player");
    let rect = PosRect(
        x - VIEW_DIST,
        y - VIEW_DIST,
        x + VIEW_DIST + 1,
        y + VIEW_DIST + 1,
    );
    Update::ViewRect(render(game, &rect))
}

